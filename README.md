# learn-programming-platform

## Require
```
- Node
- window build tool 2017
- python
- chocolatey
```
## Project setup

1. Install chocolatey Run in Powershell
```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```

2. Install window build tool 2017
```
choco install python visualstudio2017-workload-vctools -y
```
3. Node Config msvs and Python
```
npm config set python /path/to/executable/python
```
```
npm config set msvs_version 2017
```
4. Install Packages
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
